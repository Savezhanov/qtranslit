<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_document".
 *
 * @property int $id
 * @property int $user_id
 * @property string $uploaded_url
 * @property string $converted_url
 * @property string $converted_text
 * @property string $original_text
 * @property string $document_name
 *
 * @property User $user
 */
class UserDocument extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    public $img;
    public $doc;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*[['user_id', 'uploaded_url', 'converted_url'], 'required'],*/
            [['user_id'], 'integer'],
            [['uploaded_url', 'converted_url','img_url','date','document_name'], 'string', 'max' => 255],
            [['converted_text','original_text'], 'string', 'max' => 1000],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['doc'], 'file'],
            [['img'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'uploaded_url' => Yii::t('app', 'Uploaded Url'),
            'converted_url' => Yii::t('app', 'Converted Url'),
            'img_url' => Yii::t('app', 'Img Url'),
            'doc' => Yii::t('app', 'Загрузить документы'),
            'img' => Yii::t('app', 'Загрузить изображение'),
            'converted_text' => Yii::t('app', 'Конвертированный текст'),
            'original_text' => Yii::t('app', 'Оригинальный текст'),
            'document_name' => Yii::t('app', 'Название документа'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public static function findByUserId($id)
    {
        $user_document = UserDocument::find()->where(['user_id' => $id])->all();
        return $user_document;
    }
    public static function findById($id){
        $user_document = UserDocument::find()->where(['id' => $id]);
        return $user_document;
    }
}
