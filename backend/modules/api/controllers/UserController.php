<?php

namespace backend\modules\api\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use common\models\User;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

class UserController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionListUser(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $user = User::find()->all();

        if( count($user) > 0 ) {
            return array('status' => true, 'data' => $user);
        }else {
            return array('status' => false, 'data' => 'No letters found.');
        }
    }

    public function actionSignup()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

     /*   $model = new SignupForm();
        $model->attributes = Yii::$app->request->post();
        if( $model->hasErrors()){
            return $model->hasErrors();
        }else {
            return "pidt";
        }

        /*
        if( $model->hasErrors()){
            return $model->getErrors();
        }else {
            $user = $model->signup();
            return $user;
        }*/

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $username = Yii::$app->request->post('username');
        $email = Yii::$app->request->post('email');
        $password = Yii::$app->request->post('password');


        $check = User::findByEmail($email);
        if ($check == null){
            $user = new User();
            $user->username = $username;
            $user->email = $email;
            $user->setPassword($password);
            $user->generateAuthKey();
            if( $user!=null) {
                return $user->save() ? $user : null;
            }else return array('status' => false);
        }else return array('status' => false, 'data' => 'Email is already in use');


    }

    public function actionSignUps(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $model = new SignupForm();
        $model->attributes = Yii::$app->request->post();
        if( $model->hasErrors()){
            return $model->getErrors();
        }else {
            $user = $model->signup();
            return $user;
        }

    }

    public function actionLogin(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        /*$username = Yii::$app->request->post('username');*/
        $email = Yii::$app->request->post('email');
        $password = Yii::$app->request->post('password');


        $user = User::findByEmail($email);
        if(!$user || !$user->validatePassword($password)){
            return array("status" =>false);
        }else {
            return $user;
        }

    }
    public function actionChangePassword(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        /*$username = Yii::$app->request->post('username');*/
        $email = Yii::$app->request->post('email');
        $old_password = Yii::$app->request->post('old_password');
        $new_password = Yii::$app->request->post('new_password');

        $user = User::findByEmail($email);
        if(!$user || !$user->validatePassword($old_password)){
            return array("status" =>false);
        }else {
            $user->setPassword($new_password);
            $user->removePasswordResetToken();
            return $user;
        }

    }

    public function actionEditProfile(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json
        $headers = Yii::$app->request->headers;
        $headers->add('Content-Type','multipart/form-data');

        $email = Yii::$app->request->post('email');
        $password = Yii::$app->request->post('password');
        $new_username = Yii::$app->request->post('new_username');


        $user = \backend\models\User::findByEmail($email);

        if(!$user || !$user->validatePassword($password)){
            return array("status" =>false);
        }else {
            if(is_object(UploadedFile::getInstance($user, 'img'))) {
                if (!file_exists('uploads/users_profile/'.$user->email.'/')) {
                    mkdir('uploads/users_profile/'.$user->email.'/', 0777, true);
                }
                $user->img = UploadedFile::getInstance($user, 'img');
                $user->img->saveAs('uploads/users_profile/'.$user->email.'/' . $user->id . '-' . $user->created_at . '.' . $user->img->extension);

                //save the path in the db column
                $user->img_url = 'uploads/users_profile/'.$user->email.'/'.$user->id.'-'.$user->created_at.'.'.$user->img->extension;
            }

            if(isset($_POST['new_username']))
            {
                $user->username = $new_username;
            }

            $user->save();
            return $user;
        }

    }
    public function actionGetProfile(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $email = Yii::$app->request->post('email');

        $user = User::findByEmail($email);
        if($user!=null){
            return $user;
        }else {
            return array("status" =>false);
        }


    }

}
