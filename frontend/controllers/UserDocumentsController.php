<?php

namespace frontend\controllers;

use backend\models\User;
use frontend\models\UserDocument;

class UserDocumentsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $id = $this->getUser();
        $model = UserDocument::findAll("user_id = $id");
        $user = User::find()->where(['id'=>$id])->one();
        return $this->render('index',array("id" => $id,"model"=>$model,"user"=>$user));
    }

    public function getUser(){
        return \Yii::$app->user->identity->id;
    }

}
