<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?php if($model->scenario === 'loginWithEmail') { ?>
                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
            <? }else { ?>
                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <? } ?>


                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>



            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<!--
    <div class="container">
        <div class="row main">
            <div class="main-login main-center">
                <form class="form-horizontal" method="post" action="#">
                    <i class="fa fa-user-circle-o profile-img-card" aria-hidden="true"></i>
                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label">Имя пользователя</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input type="text" required  class="form-control" name="username" id="username"  placeholder="Введите имя пользователя"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="cols-sm-2 control-label">Пароль</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                <input type="password" required class="form-control" name="password" id="password"  placeholder="Введите пароль"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                        <button type="submit" class="btn login_btn anim-btn on">Войти</button>
                    </div>
                    <div class="login-register">
                        <a href="signup">Регистрация</a> или <a href="reset_password.php">забыли пароль?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
-->