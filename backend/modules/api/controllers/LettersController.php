<?php

namespace backend\modules\api\controllers;

use backend\modules\api\models\Letters;



class LettersController extends \yii\web\Controller
{
    public $enableCsrfValidation=false;

    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionCreateLetters()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $letters = new Letters();
        /*$letters->scenario = Letters::SCENARIO_CREATE;*/
        $letters->attributes = \Yii::$app->request->post();

        if($letters->validate()) {
            $letters->save();
            return array('status'=>true,'data' =>'Letter added successfully.');
        }else {
            return array('status'=>false, 'data' => $letters->getErrors() );
        }

    }
    public function actionListLetters(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $letters = Letters::find()->all();

        if( count($letters) > 0 ) {
            return array('status' => true, 'data' => $letters);
        }else {
            return array('status' => false, 'data' => 'No letters found.');
        }
    }

    /**
     * Updates an existing Letters model.
     * @param integer $id
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionEditLetters(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        /*$id = $_POST['id'];*/
        $id = 1;

        $letters = $this->findModel($id);

        if( count($letters) > 0 ) {
            $letters->scenario = Letters::SCENARIO_CREATE;
            $letters->attributes = \Yii::$app->request->post();

            if($letters->validate()) {
                $letters->save();
                return array('status'=>true,'data' =>'Letter added successfully.');
            }else {
                return array('status'=>false, 'data' => $letters->getErrors() );
            }

        }else {
            return array('status' => false, 'data' => 'No letters found.');
        }
    }

    protected function findModel($id)
    {
        if (($model = Letters::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
