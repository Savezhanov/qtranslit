<?php

namespace backend\modules\api\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use common\models\User;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use backend\models\UserDocument;
use yii\web\NotFoundHttpException;


class UserDocumentsController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionList(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $email = Yii::$app->request->post('email');
        $user_id = User::findByEmail($email)->id;
        $user_document = UserDocument::findByUserId($user_id);

        if( count($user_document) > 0 ) {
            return array('status' => true, 'data' => $user_document);
        }else {
            return array('status' => false, 'data' => 'No documents found.');
        }
    }

    public function actionAdd(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json
        $headers = Yii::$app->request->headers;
        $headers->add('Content-Type','multipart/form-data');

        //берем дату как string и записываем ее как string
        if($_POST['date'] == null){
            return array('status' => false, 'data' => 'Date is null');
        }else {
            $date = Yii::$app->request->post('date');
        }

        //берем конвертированный текст
        if( $_POST['converted_text'] == null ) {
            return array('status' => false, 'data' => 'Converted text is null');
        }else {
            $converted_text = Yii::$app->request->post('converted_text');
        }

        //берем оригинальный текст
        if( $_POST['original_text'] == null){
            return array('status' => false, 'data' => 'Original text is null');
        }else {
            $original_text = Yii::$app->request->post('original_text');
        }

        //находим пользователя по email
        if($_POST['email'] == null){
            return array('status' => false, 'data' => 'Email is null');
        }else {
            $email = Yii::$app->request->post('email');

            //находим его id чтобы записать
            $user = User::findByEmail($email);
            $user_id = $user->id;
            if ($user_id == null) {
                return array('status' => false, 'data' => 'There is no User with such email');
            } else {

                $user_document = new UserDocument();
                $user_document->user_id = $user_id;
                $user_document->document_name = "Scan " . $date;
                $user_document->converted_text = $converted_text;
                $user_document->original_text = $original_text;

                if (is_object(UploadedFile::getInstance($user_document, 'img'))) {
                    if (!file_exists('uploads/users_docs/' . $user->email . '/')) {
                        mkdir('uploads/users_docs/' . $user->email . '/', 0777, true);
                    }
                    $user_document->img = UploadedFile::getInstance($user_document, 'img');
                    $user_document->img->saveAs('uploads/users_docs/' . $user->email . '/'. 'Scan-' . $date . '.' . $user_document->img->extension);

                    //save the path in the db column
                    $user_document->img_url = 'uploads/users_docs/' . $user->email . '/' . 'Scan-' . $date . '.' . $user_document->img->extension;
                    /*return $user_document->img->baseName;*/
                }

                $user_document->save();
                if ($user_document->save()) {
                    return array('status' => true, 'data' => $user_document);
                } else {
                    return array('status' => false);
                }
            }
        }
    }
    public function actionDelete(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $id = Yii::$app->request->post('id');

        /*$user_documents = UserDocument::findById($id);*/
        $user_documents = UserDocument::find()->where(['id' => $id])->all();
        if ($id == null) {
            return array('status' => 'Empty id value');
        }else {
            if($user_documents == null) {
                return array('status' => 'There is no document with this id');
            }else {
                if( $user_documents[0]->delete() ){
                    return array('status' => true);
                }else{
                    return array('status',false);
                }
            }
        }
    }
    public function actionRename(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; //this will return response in json

        $id = Yii::$app->request->post('id');
        $new_document_name = Yii::$app->request->post('new_name');
        /*$user_documents = UserDocument::findById($id);*/
        $user_documents = UserDocument::find()->where(['id' => $id])->all();
        if ($id == null) {
            return array('status' => 'Empty id value');
        }else {
            if($user_documents == null) {
                return array('status' => 'There is no document with this id');
            }else {
                if( $user_documents[0]->delete() ){
                    return array('status' => true);
                }else{
                    return array('status',false);
                }
            }
        }
    }
}
