<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_document".
 *
 * @property int $id
 * @property int $user_id
 * @property string $uploaded_url
 * @property string $converted_url
 *
 * @property User $user
 */
class UserDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'uploaded_url', 'converted_url'], 'required'],
            [['user_id'], 'integer'],
            [['uploaded_url', 'converted_url'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'uploaded_url' => Yii::t('app', 'Uploaded Url'),
            'converted_url' => Yii::t('app', 'Converted Url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
